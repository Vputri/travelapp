package vika.ticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.widget.Toast;
import android.view.View;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Button;
import android.os.Bundle;

import vika.ticket.user.HistoryActivity;
import vika.ticket.user.HotelActivity;
import vika.ticket.user.LoginActivity;
import vika.ticket.user.ProfileActivity;
import vika.ticket.user.TrainActivity;

public class MainActivity extends AppCompatActivity {
    Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        btnLogout = findViewById(R.id.out);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Anda yakin ingin keluar ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent  = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .create();
                dialog.show();
            }
        });
    }

    public void profileMenu(View v) {
        Intent intent  = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(intent);
    }

    public void historyMenu(View v) {
        Intent intent  = new Intent(getApplicationContext(), HistoryActivity.class);
        startActivity(intent);
    }

    public void bookKereta(View v) {
        Intent intent  = new Intent(getApplicationContext(), TrainActivity.class);
        startActivity(intent);
    }

    public void bookHotel(View v) {
        Intent intent  = new Intent(getApplicationContext(), HotelActivity.class);
        startActivity(intent);
    }
}