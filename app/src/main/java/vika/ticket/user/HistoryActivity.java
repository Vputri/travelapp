package vika.ticket.user;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import vika.ticket.R;

public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getSupportActionBar().hide();
    }
}