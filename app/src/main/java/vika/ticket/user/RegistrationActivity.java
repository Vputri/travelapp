package vika.ticket.user;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;

import vika.ticket.MainActivity;
import vika.ticket.R;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.database.sqlite.SQLiteDatabase;

import com.ornach.nobobutton.NoboButton;
import android.widget.Toast;
import vika.ticket.DBHelper;


public class RegistrationActivity extends AppCompatActivity {
    Button btnBackLogin;
    EditText UserName, NamaLengkap, Email, NomorTelp, Password;
    DBHelper DB;
    NoboButton Register;
    String name, username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        getSupportActionBar().hide();

        Register = (NoboButton) findViewById(R.id.btnRegistrasi);
        btnBackLogin = findViewById(R.id.btnBackLogin);
        UserName = (EditText) findViewById(R.id.edtUserName);
        NamaLengkap = (EditText) findViewById(R.id.edtNamaLengkap);
        Email = (EditText) findViewById(R.id.edtEmail);
        NomorTelp = (EditText) findViewById(R.id.edtNomorTelp);
        Password = (EditText) findViewById(R.id.edtPassword);
        DB = new DBHelper(this);

        btnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = UserName.getText().toString();
                password = Password.getText().toString();
                Boolean checkuser = DB.checkusername(username);
                if(checkuser==false){
                    Boolean insert = DB.insertData(username, password);
                    if(insert==true){
                        Toast.makeText(RegistrationActivity.this, "Registered successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(RegistrationActivity.this, "Registration failed", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(RegistrationActivity.this, "User already exists! please sign in", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}